#!/usr/bin/env python
# coding: utf-8

# In[ ]:



# *************Eliza Chatbot*******************

#Group Member Names: 1.Saksham Arora
#                    2.Preyaa Atri
#                    
#Date: 17 September 2019
#Couses : AIT 590 FALL 2019

#Introduction  to Eliza:

#    Eliza was Created in 1966 as an early natural language processing (NLP) computer program that emulates a Rogerian 
#    psychotherapist, a clinical practice that allows clients to take more action and progression in discussions. 
#    This is also known as person-centered therapy. Developed by Joseph Weizenbaum, ELIZA, named after a character in the play 
#    Pygmalion by George Bernard Shaw, is generally known as the first chatbot.


# In[ ]:


#Importing Libraries
import time

#
import sys

#
import re

#
from nltk.chat.util import Chat, reflections

#Defining the function for Regular Expressions!!

def expression_for_conversation(person_name):
    expressions = (

        #1.Response statement for Unsafe and impetuous words:
        (r'(.*)(unsafe|disastrous|risky|murder|hazardous|harm)(.*)',
        ("Tell me more about it?”,“Can you please elucidate your thoughts?",
         "I think you need to call the National Helpline" , 
         "Please tell me, why your saying that?",
         "Please tell me, Anything I can help you in this case")),
          
        #2.Response statement for Depressive feeling:
        (r'(.*)(lousy|disappoineted|discouraged|powerless|gulty|sense\sof\sloss)(.*)',
        ("Are you capable of enjoying things right now?",
         "What’s the best way for me to help you out?",
         "Who can we call to help us get through this tough time?",
         "What do you need from me?",
         "What changes can help you feel better right now?")),
            

         #3.Response statement for Medical term:
         (r'(.*)(Alcohol|Drugs|Eating\sdisorder|Insomnia|stress|llness)(.*)',
         ("That's too bad. Have you had it long?",
          "Sorry to hear that,‘Let me know how I can help you.",
          "YOu can also get help from Life line Counseling Center")),
        
          
            
         #4.Response statement for confused:++++
         (r'(.*)(upset|doubtful|uncertain|unsure|uneasy|unbelieving|confued)(.*)',
         ("What’s the matter with you?",
          "Tell me why you think this way","Tell me more about incident,that makes you think this way",
          "can you tell me more about it")),
             
         #5.Response statement for LOVE:
         (r'(.*)(love|affectionate|admiration|devoted|in\slove|like)(.*)', 
         ("Do you believe it is normal to be in love?")),
                    
               
              
         #6.All about Relationship:             #need to update
         (r'(.*)(Friends|Best-friend|Friend)(.*)',
         ("What do you like best about your %2?",
          "Tell me more about your %2",
          "I can be your friend if you like?")),
         
         # Default Response statements
         (r'(.*)',
         ( "Do you care to explain that a little more?",
          "Can you tell me more about it",
          "Is it possible  for you to elaborate on that?",
          "How do you feel abuot it??",
          "Could you please tell me more!",
          "Is there any reason for you to say this?",
          "What goes through your mind when you say that? Can you tell me?")),

      
        
         # Relationships
         (r'(.*)(Husband|Wife|Boyfriend|Girlfriend|life\spatner|Ex\-Boyfriend)(.*)',
         ("Tell me more about your(%2)?",
          "Are you happy  with  your (%2)?",
          "Do you need any help in your relationship?")),
        
         (r'(.*)(Parent|Parents|mother|Father)(.*)',
         ("I will be happy , if you tell me more about your (%2)?",
          "How do you feel about (%2)?",
          "How often do you meet with your (%2)"))
       
    )
    return expressions




def chatBot_eliza():
    
    statement1 = "\t\t\t\tWelcome! This is the right place to get some help!!\n"
    print('\t' + '#'*110)
    
    statement2 = "A note on Positivity - \n"

    #We add a random positie quote (Some python programming required!

    statement3 = 'Hey there Amigo! To whom do I owe the pleasure of helping today?' # need to update 

    for s in statement1:
        sys.stdout.write(s)
        sys.stdout.flush()
        time.sleep(0.1)

    for s in statement2:
        sys.stdout.write(s)
        sys.stdout.flush()
        time.sleep(0.1)

    for s in statement3:
        sys.stdout.write(s)
        sys.stdout.flush()
        time.sleep(0.1)
        
    # Interaction with the user to get his/her person_name
    user_interaction_input = str(input())
    
    # In case the user wants to exit the chatbot
    if re.match(r'(Q|q)uit', user_interaction_input) is not None:
        return
    
    # In case the user wants to continue with talking to the chatbot
    else:
        interaction = re.match(r'(Hi.|Hello.)?(Hi|I am|Myself|This is|My name is)?(.*)', user_interaction_input, re.I)
        person_name = interaction.group(3)
        print("Heya", person_name +". Seems like you need my help with something? Tell me about it!!") 

        # Creation of expression for the chatbot to converse with the user
        expressions = expression_for_conversation(person_name)
        # create the chat instance by passing pairs and reflections
        conversation = Chat(expressions, reflections)
        #start conversation 
        conversation.converse()


if __name__ == "__main__":
    chatBot_eliza()


# In[ ]:




